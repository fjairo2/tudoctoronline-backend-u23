//Importación
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require("./conexion");

const app = express();
const env = process.env;
const port = env.PORT || 8080;
app.use(express.json());
app.use(morgan('dev'));
app.use(cors());
//Arranque
app.listen(port, () => {
    console.log("API iniciado en puerto " + port);
});
//Rutas base
app.get('/', (req, res) => {
    res.send("API iniciado");
});
app.use("/doctores", require("./rutas/DoctorRutas"));

app.use("/pacientes", require("./rutas/PacienteRutas"));

app.use("/agendas", require("./rutas/AgendaRutas"));

app.use("/citas",require("./rutas/CitasRutas"));

app.use("/login",require("./rutas/LoginRutas"));


