const mongoose = require("mongoose");

const pacienteSchema = mongoose.Schema({
    nombres: { type: String, maxLength: 50, required: true, unique: false },
    apellidos: { type: String, maxLength: 50, required: true, unique: false },
    documento: { type: Number, required: true, unique: true },
    direccion: { type: String, maxLength: 80, required: true, unique: false },
    telefono: { type: Number, required: true, unique: false },
    correo: { type: String, maxLength: 80, required: true, unique: true },
    password: { type: String, required: true, unique: false },
    rol: { type:String, required:true }
});

module.exports = mongoose.model("paciente", pacienteSchema);