const mongoose = require("mongoose");

const citaSchema = mongoose.Schema({
    paciente: {
        nombres: { type: String, required: true },
        apellidos: { type: String, required: true },
        documento: { type: Number, required: true }
    },
    doctor: {
        nombres: { type: String, required: true },
        apellidos: { type: String, required: true },
        especialidad: { type: String, required: true }
    },
    agenda: {
        mes: { type: String, required: true },
        fecha: { type: String, required: true }
    },
    modulo: { type: String, maxLength: 20, required: true, unique: false },
    lugar: { type: String, maxLength: 20, required: true, unique: false },
    estado: { type: String, maxLength: 20, required: true, unique: false }

});

module.exports = mongoose.model("citas", citaSchema)