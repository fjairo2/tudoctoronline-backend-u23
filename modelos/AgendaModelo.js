const mongoose = require("mongoose");

const agendaSchema = mongoose.Schema({
    mes: { type: String, maxLength: 20, required: true, unique: false },
    fecha: { type: Date, maxLength: 50, required: true, unique: false }
});

module.exports = mongoose.model("agendas", agendaSchema);