const mongoose = require("mongoose");

const username = "admin";
const password = "admin";
const database = "tuDoctorBd";
const URI = "mongodb+srv://" + username + ":" + password + "@cluster0.e4fk0ya.mongodb.net/" + database + "?retryWrites=true&w=majority";
const conectar = async () => {
    try {
        await mongoose.connect(URI);
        console.log("Atlas está en linea");
    } catch (error) {
        console.log("Error de conexión. " + error);
    }
}
conectar();

module.exports = mongoose;


/*
mongoose.connect(URI)
    .then(()=>{ console.log("Atlas está en linea"); })
    .catch(()=>{ console.log("Error de conexión. "+error); })
*/
