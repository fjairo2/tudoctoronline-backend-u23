const doctorModelo = require("../modelos/DoctorModelo");
const bcrypt=require("bcrypt");
const doctorOperaciones = {};

const cifrarPassword = async(passw)=>{
    const SALT_TIMES=10;
    const salt= await bcrypt.genSalt(SALT_TIMES);
    return await bcrypt.hash(passw, salt);
}
doctorOperaciones.crearDoctor = async (req, res) => {
    try {
        const objeto = req.body;
        objeto.password= await cifrarPassword(objeto.password);
        const doctor = new doctorModelo(objeto);
        const doctorGuardado = await doctor.save();
        res.status(201).send(doctorGuardado);
    } catch (error) {
        res.status(404).send("Mala petición. " + error);
    }
}

doctorOperaciones.buscarDoctores = async (req, res) => {
    try {
        const filtro = req.query;
        let listaDoctores;
        if (filtro.q != null) {
            listaDoctores = await doctorModelo.find({
                "$or": [
                    { "nombres": { $regex: filtro.q, $options: "i" } },
                    { "apellidos": { $regex: filtro.q, $options: "i" } }

                ]
            });
        } else {
            listaDoctores = await doctorModelo.find();
        }
        
        if (listaDoctores.length > 0) {
            res.status(200).send(listaDoctores);
        } else {
            res.status(404).send("no hay datos");
        }
    } catch (error) {
        res.status(400).send("una mala petición" + error);
    }
}

doctorOperaciones.buscarDoctor = async (req, res) => {
    try {
        const id = req.params.id;
        const doctor = await doctorModelo.findById(id);
        if (doctor != null) {
            res.status(200).send(doctor);
        } else {
            res.status(404).send("no hay datos");
        }
    } catch (error) {
        res.status(400).send("una mala petición" + error);
    }
}

doctorOperaciones.modificarDoctor = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        if(body.password != null){
            body.password = await cifrarPassword(body.password);
        }
        const datosActualizar = {
            nombres: body.nombres,
            apellidos: body.apellidos,
            especialidad: body.especialidad,
            direccion: body.direccion,
            telefono: body.telefono,
            correo: body.correo,
            password: body.password,
            rol: body.rol
        }
        const clienteActualizado = await doctorModelo.findByIdAndUpdate(id, datosActualizar, { new: true });
        if (clienteActualizado != null) {
            res.status(200).send(clienteActualizado);
        }
        else {
            res.status(200).send("No hay datos")
        }
    } catch (error) {
        res.status(400).send("Mala petición" + error);
    }
}

doctorOperaciones.borrarDoctor = async (req, res) => {
    try {
        const id = req.params.id;
        const doctor = await doctorModelo.findByIdAndDelete(id);
        if (doctor != null) {
            res.status(200).send(doctor);
        } else {
            res.status(404).send("no hay datos");
        }
    } catch (error) {
        res.status(400).send("una mala petición" + error);
    }
}

module.exports = doctorOperaciones;
