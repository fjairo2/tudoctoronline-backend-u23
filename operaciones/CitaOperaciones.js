const citaModelo = require("../modelos/CitaModelo");
citaOperaciones = {};

citaOperaciones.crearCita = async (req, res) => {
    try {
        const objeto = req.body;
        const cita = new citaModelo(objeto);
        const citaGuardada = await cita.save();
        res.status(201).send(citaGuardada);
    } catch (error) {
        res.status(404).send("Mala petición" + error);

    }
}

citaOperaciones.buscarCitas = async (req, res) => {
    try {
        const filtro = req.query;
        let listaCitas;
        if (filtro.q != null) {
            listaCitas = await citaModelo.find({
                "$or": [
                    { "estado": { $regex: filtro.q, $options: "i" } },
                    { "doctor.nombres": { $regex: filtro.q, $options: "i" }, },
                    { "doctor.apellidos": { $regex: filtro.q, $options: "i" } },
                    { "paciente.nombres": { $regex: filtro.q, $options: "i" } },
                    { "paciente.apellido": { $regex: filtro.q, $options: "i" } },
                    { "agenda.mes": { $regex: filtro.q, $options: "i" } },
                    { "agenda.fecha": { $regex: filtro.q, $options: "i" } }
                ]
            });
        } else {
            listaCitas = await citaModelo.find();
        }
        if (listaCitas.length > 0) {
            res.status(200).send(listaCitas);
        } else {
            res.status(404).send("No hay datos");
        }
        console.log(listaCitas)
    } catch (error) {
        res.status(400).send("Una mala petición" + error);
    }
}

citaOperaciones.buscarCita = async (req, res) => {
    try {
        const id = req.params.id;
        const cita = await citaModelo.findById(id);
        if (cita != null) {
            res.status(200).send(cita);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Una mala petición" + error)
    }
}

citaOperaciones.modificarCita = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;
        const datosActualiar = {
            "doctor.nombres": body.doctor.nombres,
            "doctor.apellidos": body.doctor.apellidos,
            "doctor.documento": body.doctor.documento,
            "paciente.nombres": body.paciente.nombres,
            "paciente.apellidos": body.paciente.apellidos,
            "paciente.documento": body.paciente.documento,
            "agenda.mes": body.agenda.mes,
            "agenda.fecha": body.agenda.fecha,
            "modulo": body.modulo,
            "lugar": body.lugar,
            "estado": body.estado,

        }
        const citaActualizada = await citaModelo.findByIdAndUpdate(id, datosActualiar, { new: true })
        if (citaActualizada != null) {
            res.status(200).send(citaActualizada);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

citaOperaciones.borrarCita = async (req, res) => {
    try {
        const id = req.params.id;
        const cita = await citaModelo.findByIdAndDelete(id);
        if (cita != null) {
            res.status(200).send(cita);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

module.exports = citaOperaciones;