const PacienteModelo = require("../modelos/PacienteModelo");
const DoctorModelo=require("../modelos/DoctorModelo");
const bcrypt = require("bcrypt");
const LoginOperaciones = {};


const compararPassw = async (recibido, guardado) => {
    return await bcrypt.compare(recibido, guardado);
}

LoginOperaciones.login = async(req, res) => {
    try {
        const correo = req.body.correo;
        let password = req.body.password;
        var usuario = await PacienteModelo.findOne({correo: correo});
        if(usuario == null){
            usuario = await DoctorModelo.findOne({correo: correo});
        }
        if (usuario != null) {
            const result = await compararPassw(password , usuario.password);
            if (result) {
                const acceso = {
                    nombres: usuario.nombres+" "+usuario.apellidos,
                    rol: usuario.rol,
                    //token: generarToken(usuario.id, usuario.nombres+" "+usuario.apellidos, usuario.es_admin)
                }
                res.status(200).json(acceso);
            }
            else {
                res.status(401).send("Email o contraseña incorrectos");    
            }
        }
        else {
            res.status(401).send("Email o contraseña incorrectos");
        }
    } catch (error) {
        res.status(400).json(error);
    }
}

module.exports=LoginOperaciones;
