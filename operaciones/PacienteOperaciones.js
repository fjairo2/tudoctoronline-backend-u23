const pacienteModelo = require("../modelos/PacienteModelo");
const bcrypt = require("bcrypt");
const pacienteOperaciones = {};

const cifrarPassword = async (passw) => {
    const SALT_TIMES = 10;
    const salt = await bcrypt.genSalt(SALT_TIMES);
    return await bcrypt.hash(passw, salt);
}

pacienteOperaciones.crearPaciente = async (req, res) => {
    try {
        const objeto = req.body;
        objeto.password = await cifrarPassword(objeto.password);
        const paciente = new pacienteModelo(objeto);
        const pacienteGuardado = await paciente.save();
        res.status(201).send(pacienteGuardado);
    } catch (error) {
        res.status(404).send("Mala Petición. " + error);
    }
}

pacienteOperaciones.buscarPacientes = async (req, res) => {
    try {
        const filtro = req.query;
        let listaPacientes;
        if (filtro.q != null) {
            listaPacientes = await pacienteModelo.find({
                "$or": [
                    { "nombres": { $regex: filtro.q, $options: "i" } },
                    { "apellidos": { $regex: filtro.q, $options: "i" } }
                ]
            });
        } else {
            listaPacientes = await pacienteModelo.find();
        }

        if (listaPacientes.length > 0) {
            res.status(200).send(listaPacientes);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Una mala petición" + error)
    }
}

pacienteOperaciones.buscarPaciente = async (req, res) => {
    try {
        const id = req.params.id;
        const Paciente = await pacienteModelo.findById(id);
        if (Paciente != null) {
            res.status(200).send(Paciente);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Una mala petición" + error)
    }
}

pacienteOperaciones.modificarPaciente = async (req, res) => {
    try {
        const id = req.params.id;
        const body = req.body;

        if (body.password != null) {
            body.password = await cifrarPassword(body.password)
        }
        const datosActualiar = {
            nombres: body.nombres,
            apellidos: body.apellidos,
            direccion: body.direccion,
            telefono: body.telefono,
            correo: body.correo,
            password: body.password,
            rol: body.rol
        }
        const pacienteActualizado = await pacienteModelo.findByIdAndUpdate(id, datosActualiar, { new: true })
        if (pacienteActualizado != null) {
            res.status(200).send(pacienteActualizado);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

pacienteOperaciones.borrarPaciente = async (req, res) => {
    try {
        const id = req.params.id;
        const paciente = await pacienteModelo.findByIdAndDelete(id);
        if (paciente != null) {
            res.status(200).send(paciente);
        } else {
            res.status(404).send("No hay datos");
        }
    } catch (error) {
        res.status(400).send("Mala petición. " + error);
    }
}

module.exports = pacienteOperaciones;