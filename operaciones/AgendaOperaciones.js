const agendaModelo = require("../modelos/AgendaModelo");
const agendaOperaciones = {};

agendaOperaciones.crearAgenda = async (req, res) => {
    try {
        const objeto = req.body;
        const agenda = new agendaModelo(objeto)
        const agendaGuardada = await agenda.save();
        res.status(201).send(agendaGuardada);
    } catch (error) {
        res.status(404).send("Mala petición" + error);
    }
}

agendaOperaciones.buscarAgendas = async (req, res) => {
    try {
        const filtro = req.query;
        let listaAgenda;
        if (filtro.q != null) {
            listaAgenda = await agendaModelo.find({
                "$or": [
                    { "mes": { $regex: filtro.q, $options: "i" } },
                    { "fecha": { $regex: filtro.q, $options: "i" } }
                ]
            });
        } else {
            listaAgenda = await agendaModelo.find();
        }
        if (listaAgenda.length > 0) {
            res.status(200).send(listaAgenda);
        } else {
            res.status(404).send("no hay datos");
        }
    } catch (error) {
        res.status(400).send("Ina mala petición" + error)
    }
}

module.exports = agendaOperaciones;

