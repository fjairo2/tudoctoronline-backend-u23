const citaOperaciones = require("../operaciones/CitaOperaciones");
const router=require("express").Router();

router.get("/", citaOperaciones.buscarCitas);
router.get("/:id", citaOperaciones.buscarCita);
router.post("/",citaOperaciones.crearCita);
router.put("/:id", citaOperaciones.modificarCita);
router.delete("/:id", citaOperaciones.borrarCita)



module.exports=router;