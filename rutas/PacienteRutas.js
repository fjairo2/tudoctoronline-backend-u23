const pacienteOperaciones = require("../operaciones/PacienteOperaciones");
const router = require("express").Router();

router.get("/", pacienteOperaciones.buscarPacientes);
router.get("/:id", pacienteOperaciones.buscarPaciente);
router.post("/", pacienteOperaciones.crearPaciente);
router.put("/:id", pacienteOperaciones.modificarPaciente);
router.delete("/:id", pacienteOperaciones.borrarPaciente)

module.exports = router;