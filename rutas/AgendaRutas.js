const agendaOperaciones = require("../operaciones/AgendaOperaciones");
const router = require("express").Router();

router.get("/", agendaOperaciones.buscarAgendas);
router.post("/", agendaOperaciones.crearAgenda);

module.exports = router;